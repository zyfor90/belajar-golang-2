package github

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCraeteRepoRequestAsJson(t *testing.T) {
	request := CreateRepoRequest{
		Name:        "Testing API",
		Description: "This repository is testing",
		Homepage:    "https://github.com",
		Private:     false,
		HasIssues:   false,
		HasProjects: false,
		HasWiki:     false,
	}

	// Marhsal Take an input interface and attemped to create a valid json string
	bytes, err := json.Marshal(request)
	assert.Nil(t, err)
	assert.NotNil(t, bytes)

	fmt.Println(string(bytes))
}
